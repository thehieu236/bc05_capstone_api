let BASE__URL = 'https://635f4b14ca0fe3c21a991c87.mockapi.io/';
let arrItem = [];
let idEdited = null;

disabled('updateProduct', 'none');
function fetchAllProduct() {
	turnOnLoading();
	axios({
		url: `${BASE__URL}/QuanTri`,
		method: 'GET',
	})
		.then((res) => {
			arrItem.push(res.data);
			turnOffLoading();
			renderProductList(res.data);
		})
		.catch((err) => {
			turnOffLoading();
			console.log(err);
		});
}

fetchAllProduct();
console.log(arrItem);
function addProduct() {
	let data = layThongTinTuForm();
	let newProduct = {
		name: data.name,
		price: data.price,
		image: data.image,
		desc: data.desc,
	};
	let validate = true;
	// Validate
	validate = kiemTraRong() && kiemTraGia();

	if (validate) {
		turnOnLoading();
		axios({
			url: `${BASE__URL}/QuanTri`,
			method: 'POST',
			data: newProduct,
		})
			.then(() => {
				turnOffLoading();
				fetchAllProduct();
			})
			.catch((err) => {
				turnOffLoading();
				console.log('err: ', err);
			});
	} else {
		turnOffLoading();
		return;
	}
}

function removeProduct(id) {
	turnOnLoading();
	axios({
		url: `${BASE__URL}/QuanTri/${id}`,
		method: 'DELETE',
	})
		.then((res) => {
			fetchAllProduct();
			turnOffLoading();
		})
		.catch((err) => {
			console.log(err);
			turnOffLoading();
		});
}

function editProduct(idProduct) {
	turnOnLoading();
	disabled('addProduct', 'none');
	disabled('updateProduct', 'block');
	axios({
		url: `${BASE__URL}/QuanTri/${idProduct}`,
		method: 'GET',
	})
		.then((res) => {
			idEdited = res.data.id;
			document.getElementById('name__product').value = res.data.name;
			document.getElementById('price__product').value = res.data.price;
			document.getElementById('image__product').value = res.data.image;
			document.getElementById('desc__product').value = res.data.desc;
			turnOffLoading();
		})
		.catch((err) => {
			console.log(err);
			turnOffLoading();
		});
}

function updateProduct() {
	turnOnLoading();
	let data = layThongTinTuForm();

	// Validate
	validate = kiemTraRong() && kiemTraGia();

	if (validate) {
		turnOnLoading();
		axios({
			url: `${BASE__URL}/QuanTri/${idEdited}`,
			method: 'PUT',
			data: data,
		})
			.then(() => {
				fetchAllProduct();
				turnOffLoading();
			})
			.catch((err) => {
				console.log(err);
				turnOffLoading();
			});
	} else {
		turnOffLoading();
		return;
	}
}
